import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Route, Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {MatSelectChange} from '@angular/material/select';
import Swal from "sweetalert2";
export const formErrors: { [key: string]: string } = {
  required: 'champ obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Le mot de passe doit contenir au moins 8 caractères.\n.',
  minLengthPhone: 'Le numéro de téléphone doit contenir au moins 8 caractères.\n.',
  email:'format email est invalid \n',
  mismatch: 'Les mots de passe ne correspondent pas\n.',
  unique: 'Les mots de passe doivent contenir au moins 3 caractères uniques.\n.'
};
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm:FormGroup;
  statErreur:boolean=false;
  poste:any;
  listRole:any[]=["Admin","RH","ChefDepartement"];
  private errors: string[];
    IsWait:any=false;
  constructor(private  _fb:FormBuilder,private authService:AuthService
  ,
              private router:Router) {



  }
  annuler(){
    this.loginForm.reset();
  }

  RoleSelected($event: MatSelectChange) {
    this.poste=$event.source.value;
    console.log("role is ==>"+this.poste);
  }
login(){
    if(this.loginForm.valid){
      console.log(this.loginForm.value);

      let userobject:any={
        "username":this.loginForm.value["username"],
        "password":this.loginForm.value["password"],
        "poste":this.poste
      };
	  this.IsWait=true;
      this.authService.auth(userobject).toPromise().then(res=>{
        console.log("resultat auth"+JSON.stringify(res));
        localStorage.setItem("username",this.loginForm.value["username"]);
        localStorage.setItem("password",this.loginForm.value["password"]);
        localStorage.setItem("poste",this.poste);
        
        this.router.navigate(["/dashboard"]);

      },error=>{
        this.IsWait=false;
        Swal.fire({
          title: 'info incorrect ',
          text: 'probleme d authentification',
          icon: 'warning',
        });
        console.log("error auth"+JSON.stringify(error));

      })


    }
    else {
      this.statErreur=true;
      this.errors=["not valid"]
    }


}
  ngOnInit() {
    document.body.classList.add('bg-img');
    this.loginForm=this._fb.group({
      username:["",Validators.compose([Validators.required])],
      password: ["",Validators.compose([Validators.minLength(8),Validators.required])]
    })

  }


}
