import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { LoginPageComponent } from './login-page.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatFormFieldModule} from '@angular/material/form-field';
import {NbCardModule, NbThemeModule} from '@nebular/theme';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatInputModule} from '@angular/material/input';
import {HttpClientModule} from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent
  }
];

@NgModule({
  declarations: [LoginPageComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    NgxSpinnerModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSelectModule,
    MatRadioModule,
    NbThemeModule,
    NgbModule,
    NbCardModule,
    NgxSpinnerModule,
    MatInputModule,
    HttpClientModule,
    FormsModule,  NbThemeModule,
    ReactiveFormsModule,ReactiveFormsModule,
    MatIconModule,
    MatRadioModule,
    MatButtonModule,
    MatProgressSpinnerModule


  ]
})
export class LoginPageModule { }
