import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {AuthService} from "./auth.service";
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  test:any;
  constructor( private router: Router) {
  }

  canActivate(): boolean {
    let username= localStorage.getItem("username");
    let password= localStorage.getItem("password");
    let poste=localStorage.getItem("poste");
    if (!username && !password && !poste) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    return true;

  }
}
