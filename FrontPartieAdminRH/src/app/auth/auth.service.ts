import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {User} from '../models/user';
import {environment} from '../../environments/environment';


@Injectable({ providedIn: 'root' })
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private baseUrl = 'http://localhost:9093/user';
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string,poste:string ) {
    let userobject:any={
      "username":username,
      "password":password,
      "role":poste
    };


  }

  auth(user:any):Observable<Object> {
    return this.http.post(`${this.baseUrl}/authplatform`,user);
  }



  logout() {
    // remove user from local storage to log user out
    localStorage.clear();
  }
    /*
      login(username: string, password: string,role:string ) {
        return this.http.post<any>(`${environment.apiUrl}/authplatform`, { username, password })
          .pipe(map(user => {
            // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
            user.authdata = window.btoa(username + ':' + password);
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);

            return user;
          }));
      }*/

  }
