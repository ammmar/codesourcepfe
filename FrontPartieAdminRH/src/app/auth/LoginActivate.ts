import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
@Injectable()
export class LoginActivate implements CanActivate {
  constructor( private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    let user=(localStorage.getItem('currentUser'));
    let role= JSON.parse( localStorage.getItem("role"));

    if (user) {
      console.log("no user");
      this.router.navigate(['/dashboard/']);
      return  false;
      /*if(role==="chef"){
        this.router.navigate(['/dashboard/']);
        return false;
      }
      else if(role=="dev")
      {
        this.router.navigate(['/dashboard/d']);
        return false;
      }
      else if(role=="client")
      {
        this.router.navigate(['/dashboard/client/']);
        return false;
      }*/
    }
    console.log("exist user");

    return true;
  }
}
