import { TestBed } from '@angular/core/testing';

import { SimulerTestService } from './simuler-test.service';

describe('SimulerTestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimulerTestService = TestBed.get(SimulerTestService);
    expect(service).toBeTruthy();
  });
});
