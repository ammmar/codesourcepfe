import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionquestionComponent } from './gestionquestion.component';

describe('GestionquestionComponent', () => {
  let component: GestionquestionComponent;
  let fixture: ComponentFixture<GestionquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
