import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';
import Swal from "sweetalert2";
import { Option, Question, Quiz, QuizConfig } from '../../../models/index';
import {SharedService} from '../../../services/shared.service';
import {Router} from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-voir-resultat',
  templateUrl: './voir-resultat.component.html',
  styleUrls: ['./voir-resultat.component.scss']
})
export class VoirResultatComponent implements OnInit {
questionList :[]=[];
printmode:Boolean=true;
score:string;
dateEval:string;
name:string;
theme:string;

  constructor(private sharedService:SharedService,private router:Router) {
    this.score=localStorage.getItem("score");
    this.dateEval=localStorage.getItem("dateEval");
    this.name=localStorage.getItem("name");
    this.theme=localStorage.getItem("theme");

    this.afficheMsg("********* LocalStorage  name  "+name+" score   "+this.score+"  date Eval"+this.dateEval);



  }

  ngOnInit() {

    this.sharedService.messageSource.subscribe(m=>{
      this.questionList=JSON.parse(m);
     // this.afficheMsg("resultat"+m);
/*
      for(let i=0;i<this.questionList.length;i++) {
        let q = this.questionList[i];
        this.afficheMsg("name question "+q["name"]);
      }*/

      /*for(let i=0;i<this.questionList.length;i++){
        let q=this.questionList[i];
        this.afficheMsg("name question "+q["name"]);
      }*/

    })
  }
  NavToEtat(){
    localStorage.clear();
    this.router.navigate(["etatTest"])
  }

print(){
  var innerContents = document.getElementById("print-section").innerHTML;
  var popupWinindow = window.open('',
    '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no');
  popupWinindow.document.open();
  popupWinindow.document.write('<html><head>' +
    '<link rel="stylesheet" type="text/css" /> ' +
    '  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">' +
    '  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>' +
    '  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>' +

    '</head><body onload="window.print()">' + innerContents + '</html>');
  popupWinindow.document.close();



}


  isCorrect(question: Question) {
    return question.options.every(x => x.selected === x.answer) ? 'correct' : 'fausse';
  };

  CorrectOption(option:Option){

    if(option.selected===true && option.answer===option.selected){
      return  true;
    }
    return false;
  }

  afficheMsg(msg:any){
    console.log("************Voir resultat "+msg);
  }

}
