import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {UserService} from '../../../services/user.service';
import {SharedService} from '../../../services/shared.service';
export const formErrors: { [key: string]: string } = {
  required: 'cette champs est obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Password must contain at least 8 characters.',
  minLengthPhone: 'Phone Number must contain at least 8 characters.',

  mismatch: 'Passwords don\'t match.',
  unique: 'Passwords must contain at least 3 unique characters.'
};

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  minPw = 8;

  today = new Date();
  poste;
  email: any;
  showMessages: any = {};
  submitted: boolean = false;
  listRole: any[] = ["Admin", "RH", "ChefDepartement"];
  validation = {};
  updateUser: FormGroup;
  user: any;
  formErrors=formErrors;

 public  default="";
  username = new FormControl('', [Validators.required]);
  firstname = new FormControl('', [Validators.required]);
  lastname = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);
  usertel = new FormControl('', [Validators.required]);
  useradresse = new FormControl('', [Validators.required]);

  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(private  userService: UserService, private sharedService: SharedService,
              private router: Router, private activatedRoute: ActivatedRoute, private  fb: FormBuilder) {

    this.sharedService.messageSource.subscribe(m => {

      console.log("resultat is " + JSON.stringify(m));
      this.user = JSON.parse(JSON.stringify(m));
    this.default=this.user["poste"];

      /*for(let i=0;i<this.questionList.length;i++){
        let q=this.questionList[i];
        this.afficheMsg("name question "+q["name"]);
      }*/

    })

  }
// Validators.minLength(this.minPw)

  ngOnInit() {
    this.updateUser = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      usertel: ['', Validators.required],
      useradresse: ['', Validators.required],

    });
  }

  OnReset() {
    this.updateUser.reset();
  }

  NavToList() {
    this.router.navigate(["manage-user"])
  }


  RoleSelected($event: MatSelectChange) {
    this.poste = $event.source.value;
    console.log("role is ==>" + this.poste);
  }

  onSubmit() {
    if (this.updateUser.valid) {
      this.updateUser.value["userdate"] = this.today;
      this.updateUser.value["lastPasswordResetDate"] = this.today;
      this.updateUser.value["disponibilite"] = true;
      this.updateUser.value["enabled"] = true;
      this.updateUser.value["poste"] = this.poste;
      console.log("************id user is " + this.user.id)
      this.userService.updateUser(this.updateUser.value, this.user.id).toPromise().then(reponse => {
        Swal.fire({
          title: 'update user success  ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(["manage-user"])
      }, error => {
        Swal.fire({
          title: 'failed update user  ',
          text: 'failed add user',
          icon: 'warning',
        });
      });


    }

  }
}


