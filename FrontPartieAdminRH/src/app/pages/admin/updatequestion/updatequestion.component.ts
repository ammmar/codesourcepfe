import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {QuestionService} from '../../../services/question.service';

@Component({
  selector: 'app-updatequestion',
  templateUrl: './updatequestion.component.html',
  styleUrls: ['./updatequestion.component.scss']
})
export class UpdatequestionComponent implements OnInit {

  updateform: FormGroup;
  idQuestion: any;
  Question: any;


  desgnQuestion = new FormControl('', [Validators.required]);
  tempsQuestion= new FormControl('', [Validators.required]);
  unite=new FormControl('', [Validators.required]);
  theme=new FormControl('', [Validators.required]);
  desgnQuestions:any="";
  tempsQuestions:any="";
  unites:any="";
  themes:any="Aucune Valeur";

  constructor(private httpClient: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private  fb: FormBuilder, private questionService: QuestionService) {
    this.idQuestion = this.activatedRoute.snapshot.params['id'];
    console.log("**********UpdateQuestion: id  routed  " + this.idQuestion);

    this.questionService.getQuestionById(this.idQuestion).toPromise().then(response=>{
      console.log("UpdateQuestion   response is "+response);
      this.Question=JSON.parse(JSON.stringify(response));
      this.desgnQuestions=this.Question['desgnQuestion'];
      this.themes=this.Question["theme"];
      this.tempsQuestions=this.Question["tempsQuestion"]
      this.unites=this.Question["unite"];
      console.log("question  theme "+this.Question['desgnQuestion']+"\t themee"+this.Question["theme"])

    },error=>{
      console.log("UpdateQuestion:Error"+JSON.stringify(error));
    });
  }

  ngOnInit() {
    this.updateform = this.fb.group({
      desgnQuestion: ['', Validators.required],
      tempsQuestion: ['', Validators.required],
      theme:[,Validators.required],
      unite: ['', Validators.required],

    });
  }
  NavtoListQuestion(){

     let idquestion=localStorage.getItem("idquestion");
     let idTestQuestion=localStorage.getItem("idTestQuestion");
    let idTestaffecter=localStorage.getItem("idTestaffecter");
    localStorage.clear();
    if(idTestQuestion!==null){
      this.router.navigate(["/list-question-select", idTestQuestion])

    }
    if(idTestaffecter!==null){
      this.router.navigate(["/list-question-select", 2])
    }
  }

  onSubmit() {
   if (this.updateform.valid) {
      console.log("valide form");
      this.updateform.value["idQuestion"]=this.idQuestion;
      this.questionService.creerModifierQuestion(this.updateform.value).toPromise().then(reponse=>{
        Swal.fire({
          title: 'modifier question success  ',
          text: 'success',
          icon: 'success',
        });
        this.NavtoListQuestion();
      //  this.router.navigate(['/question-test',this.idQuestion])
        console.log("UpdateQuestion:Sucess"+JSON.stringify(reponse));
      },error=>{

        Swal.fire({
          title: 'modifier test echec  ',
          text: 'failure',
          icon: 'warning',
        });
          this.NavtoListQuestion();
      }


    // console.log("UpdateQuestion:error"+JSON.stringify(error));

   )



   }
   else{
    console.log("invalid update question");
   }

  }
}
