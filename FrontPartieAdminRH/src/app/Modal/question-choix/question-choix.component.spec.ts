import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionChoixComponent } from './question-choix.component';

describe('QuestionChoixComponent', () => {
  let component: QuestionChoixComponent;
  let fixture: ComponentFixture<QuestionChoixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionChoixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionChoixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
