import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailComposeDialogComponent } from './mail-compose-dialog.component';

describe('MailComposeDialogComponent', () => {
  let component: MailComposeDialogComponent;
  let fixture: ComponentFixture<MailComposeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailComposeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailComposeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
