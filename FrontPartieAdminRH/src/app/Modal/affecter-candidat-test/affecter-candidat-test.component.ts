import {Component, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";

import {TestService} from '../../services/test.service';

@Component({
  selector: 'app-affecter-candidat-test',
  templateUrl: './affecter-candidat-test.component.html',
  styleUrls: ['./affecter-candidat-test.component.scss']
})
export class AffecterCandidatTestComponent implements OnInit {

  affecterFormCandidat:FormGroup;
 // id_test:any;
  id_test:any;
  idCandiat:any;
  //nameTest:any;
  listTest:[]=[];
  today=new Date();
  isAfter:any;
  nameControl = new FormControl('');
  name="";
  prenom="";
  dateDebut= new FormControl('', [Validators.required, Validators.email]);
  dateFin= new FormControl('', [Validators.required, Validators.email]);



  constructor(public dialogRef: MatDialogRef<AffecterCandidatTestComponent>,
              @Inject(MAT_DIALOG_DATA) public data:any,private  fb: FormBuilder,
              private testService:TestService)
  {
    //this.nameTest=data.name;
  //  this.id_test=data.id_test;
    this.idCandiat=this.data.id_candidat;
    this.name=this.data.name;
    this.prenom=this.data.prenom;


this.testService.getallTest().toPromise().then(reponse=>{
  this.afficherMessge("list test "+JSON.stringify(reponse));
  this.listTest=JSON.parse(JSON.stringify(reponse));
},error=>{
  this.afficherMessge("list test error "+JSON.stringify(error));
})
  }

  ngOnInit() {
    this.affecterFormCandidat=this.fb.group({
      dateDebut:['',Validators.required],
      dateFin:['',Validators.required]
    });
  }


  affecterCandidat(){
    if(this.id_test>0 && this.idCandiat>0) {

      if (this.affecterFormCandidat.valid) {
        let test={dateDebut:this.affecterFormCandidat.value["dateDebut"],dateFin:this.affecterFormCandidat.value["dateFin"]};
        this.afficherMessge("date debut" + this.affecterFormCandidat.value["dateDebut"]+"id test "+this.id_test+
        "id candidat "+this.idCandiat+" test ");
        this.testService.affecterTestCandidat(this.id_test, this.idCandiat, test).toPromise().then(reponse => {

          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'le candiat est affecté',
            showConfirmButton: false,
            timer: 1500
          })
        }, error => {
          Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: 'probleme d affectation',
            showConfirmButton: false,
            timer: 1500
          })
        })
      }
      this.dialogRef.close();
    }else{
      this.afficherMessge("acune id test et candiat exist"    );
    }
  }

  testSelected($event: MatSelectChange) {
    this.id_test=$event.source.value;
    this.afficherMessge("id test    "+$event.source.value);
  }
  OnReset(){
    this.dialogRef.close();

  }


  afficherMessge(msg:any){
    console.log("affecterTest modal"+msg);
  }
}
