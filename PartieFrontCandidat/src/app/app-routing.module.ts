import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InscritCandidatComponent} from "./pages/inscrit-candidat/inscrit-candidat.component";
import {PasserTestComponent} from "./pages/passer-test/passer-test.component";
import {ResultatComponent} from "./pages/resultat/resultat.component";


const routes: Routes = [
  {path:'inscrit',component:InscritCandidatComponent},
  {path:'passerTest/:id',component:PasserTestComponent},
  {path:'resultat',component:ResultatComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
