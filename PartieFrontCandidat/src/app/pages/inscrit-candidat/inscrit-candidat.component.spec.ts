import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscritCandidatComponent } from './inscrit-candidat.component';

describe('InscritCandidatComponent', () => {
  let component: InscritCandidatComponent;
  let fixture: ComponentFixture<InscritCandidatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscritCandidatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscritCandidatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
