import { Component, OnInit } from '@angular/core';


import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {CandidatService} from "../../services/candidat.service";
@Component({
  selector: 'app-inscrit-candidat',
  templateUrl: './inscrit-candidat.component.html',
  styleUrls: ['./inscrit-candidat.component.css']
})
export class InscritCandidatComponent implements OnInit {
  addCandidat:FormGroup;
  today=new Date();
  constructor(private candidatService:CandidatService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {

  }

  ngOnInit() {


    this.addCandidat=this.fb.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      email:['',Validators.email],
      ecole:['',Validators.required],
      password:['',Validators.required],
      tel:['',Validators.required],
      experience:['',Validators.required],
      niveau:['',Validators.required],
      profil:['',Validators.required],
      dateDebot:['',Validators.required]
    });
  }

  onSubmit(){
    if(this.addCandidat.valid)
    {
      this.afficheMsq("Valid form "+this.addCandidat.value["nom"]);
      this.candidatService.creerCandidat(this.addCandidat.value).toPromise().then(response=>{
        this.afficheMsq("add candiat "+JSON.stringify(response));
        Swal.fire({
          title: 'add candidat success  ',
          text: 'success',
          icon: 'success',
        });
        this.addCandidat.reset();
        this.router.navigate(['/candidat/']);
      },error=>{
        Swal.fire({
          title: 'add candidat failure ',
          text: 'probleme d ajout candidat',
          icon: 'warning',
        });
        console.log("addTest:Erreur is =========>***"+JSON.stringify(error));
      });
      this.addCandidat.reset();

      localStorage.setItem("nomcandidat",this.addCandidat.value["nom"]+"   "+this.addCandidat.value["prenom"])

      localStorage.setItem("inscrit","itrue");

      this.router.navigate(["resultat"]);
    }
  }
  NavToList(){
    this.router.navigate(['/candidat/']);

  }

  afficheMsq(msg:any)
  {
    console.log("add-candiat      "+msg);
  }


}
