import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/*
MatButtonModule,
  MatCardModule,

  MatDatepickerModule,
  MatDividerModule, MatExpansionModule,
  MatFormFieldModule, MatIconModule,
  MatSelectModule,
  MatInputModule, MatNativeDateModule, MatSlideToggleModule, MatTooltipModule
 */

import {MatAutocompleteModule} from "@angular/material/autocomplete";
import{MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatExpansionModule} from "@angular/material/expansion";
import { InscritCandidatComponent } from './pages/inscrit-candidat/inscrit-candidat.component';
import {CandidatService} from "./services/candidat.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {NbCardModule} from "@nebular/theme";
import { PasserTestComponent } from './pages/passer-test/passer-test.component';
import {TestService} from "./services/test.service";
import {SimulerTestService} from "./services/simuler-test.service";
import { ResultatComponent } from './pages/resultat/resultat.component';

@NgModule({
  declarations: [
    AppComponent,
    InscritCandidatComponent,
    PasserTestComponent,
    ResultatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatDividerModule, MatExpansionModule,
    MatFormFieldModule, MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTooltipModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NbCardModule



  ],
  providers: [CandidatService,TestService,SimulerTestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
