export class Test {
  idTest: number;
  dateDebut: string;
  dateEvaluation: any;
  dateFin: string;
  description: string;
  etatTest: number;
  logo: any;
  score: number;
  numTel:string;
  theme: string;
  resultat: string;
  typeTest: string;
  etatTestInfo:String;
  nomcandidat: string;
  id_candidat: number;
  prenomcandidat: string;

}
