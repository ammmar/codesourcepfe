-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 17 juil. 2020 à 22:28
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `medianetbackend`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `FINDDurationMinute`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `FINDDurationMinute` (IN `idtest` INT(100) UNSIGNED)  SELECT sum(q.temps_question) from testquestion  NATURAL join question q          WHERE testquestion.id_test=idtest$$

DROP PROCEDURE IF EXISTS `getNumberCandidatByMonth`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getNumberCandidatByMonth` (IN `month` VARCHAR(100))  SELECT  count(month)
from candidat
WHERE  candidat.month=month$$

DROP PROCEDURE IF EXISTS `NbrTestFailed`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NbrTestFailed` ()  SELECT COUNT(theme)

from test
WHERE etat_test=1 and score<49$$

DROP PROCEDURE IF EXISTS `NbrTestSuccess`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NbrTestSuccess` ()  SELECT COUNT(theme)

from test
WHERE etat_test=1 and score>49$$

DROP PROCEDURE IF EXISTS `TestNotPassed`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TestNotPassed` ()  select count(theme)

FROM test

where etat_test=0$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `authority`
--

DROP TABLE IF EXISTS `authority`;
CREATE TABLE IF NOT EXISTS `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `authority_seq`
--

DROP TABLE IF EXISTS `authority_seq`;
CREATE TABLE IF NOT EXISTS `authority_seq` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `authority_seq`
--

INSERT INTO `authority_seq` (`next_val`) VALUES
(14);

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

DROP TABLE IF EXISTS `candidat`;
CREATE TABLE IF NOT EXISTS `candidat` (
  `id_candidat` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `ecole` varchar(200) NOT NULL,
  `experience` varchar(200) NOT NULL,
  `Niveau` int(11) NOT NULL,
  `profil` varchar(20) NOT NULL,
  `date_debot` date NOT NULL DEFAULT current_timestamp(),
  `cv` blob DEFAULT NULL,
  `date_Evaluation` date DEFAULT NULL,
  `month` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_candidat`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `candidat`
--

INSERT INTO `candidat` (`id_candidat`, `nom`, `prenom`, `email`, `tel`, `ecole`, `experience`, `Niveau`, `profil`, `date_debot`, `cv`, `date_Evaluation`, `month`, `password`) VALUES
(2, 'hamza', 'ammar', 'ammar.hamza1995@gmail.com', '53652677', 'isaam', '3', 4, 'front End', '2020-04-16', NULL, NULL, 'Avril', ''),
(3, 'sihem', 'nasroui', 'nasroui@yahoo.fr', '25698741', 'ENSI', '1', 5, 'Backend ', '2020-02-04', NULL, NULL, 'Fevrier', ''),
(4, 'bargawi', 'asma', 'ammar.asma1995@gmail.com', '25698747', 'isaam', '2', 5, 'front End', '2020-02-03', NULL, NULL, 'Fevrier', ''),
(17, 'mohamed', 'ben hossin', 'mohamed@yahoo.fr', '50959590', 'iset', '3', 4, 'design', '2020-02-04', NULL, NULL, 'Fevrier', ''),
(18, 'arij', 'tebei', 'arij123@sofrecom.com', '25241789', 'isaam', '1', 5, 'designer', '2020-04-09', NULL, NULL, 'Avril', ''),
(19, 'houssem', 'ammar', 'housem@simpleon', '22237416', 'ENSI', '2', 5, 'front', '2020-05-30', NULL, NULL, 'Mai', ''),
(20, 'mostafa', 'rebei', 'mosatfa@yahoo.fr', '22247152', 'isaam', '10', 5, 'design', '2020-02-11', NULL, NULL, 'Fevrier', '');

-- --------------------------------------------------------

--
-- Structure de la table `candidat_reponse_choix`
--

DROP TABLE IF EXISTS `candidat_reponse_choix`;
CREATE TABLE IF NOT EXISTS `candidat_reponse_choix` (
  `id_crc` int(11) NOT NULL AUTO_INCREMENT,
  `id_reponse` int(11) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  `id_choix` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_crc`),
  KEY `id_reponse` (`id_reponse`),
  KEY `id_candidat` (`id_candidat`),
  KEY `id_choix` (`id_choix`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `choix`
--

DROP TABLE IF EXISTS `choix`;
CREATE TABLE IF NOT EXISTS `choix` (
  `id_choix` int(11) NOT NULL AUTO_INCREMENT,
  `choix` text NOT NULL,
  `reponse` int(1) NOT NULL,
  `id_question` int(11) NOT NULL,
  PRIMARY KEY (`id_choix`),
  KEY `id_question` (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=444 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `choix`
--

INSERT INTO `choix` (`id_choix`, `choix`, `reponse`, `id_question`) VALUES
(6, '<p>', 0, 4),
(7, '<h1>', 0, 4),
(8, '<h2>', 1, 4),
(20, '<footer>', 0, 3),
(21, 'angular', 0, 5),
(22, 'react', 0, 5),
(23, 'vue', 1, 5),
(24, '<title>', 0, 3),
(25, '<html>', 1, 3),
(26, '<p>', 0, 3),
(27, 'join', 1, 511),
(28, 'where', 0, 511),
(29, 'having', 0, 511),
(30, 'relationnel', 1, 144),
(31, 'no relationnel', 0, 144),
(32, 'fichier', 0, 144),
(33, '<h5>', 0, 3),
(34, 'dépassement memoire ', 0, 4281),
(35, 'variable inconnu', 1, 4281),
(36, 'bibliothèque ', 0, 4281),
(37, 'langage', 0, 7021),
(38, 'satandard', 1, 7021),
(39, 'varaible', 0, 7021),
(40, '<thead>', 0, 3),
(41, 'allocation mémoire', 1, 9415),
(42, 'bibliothèque graphique ', 0, 9415),
(43, 'pointeur', 0, 9415),
(44, 'LDD', 0, 8157),
(45, '<h2>', 0, 3),
(351, '<p>', 0, 350),
(352, '<h1>', 0, 350),
(353, '<h2>', 0, 350),
(358, 'langage', 0, 357),
(359, 'satandard', 0, 357),
(360, 'varaible', 0, 357),
(404, '<footer>', 0, 403),
(405, '<title>', 0, 403),
(406, '<html>', 0, 403),
(407, '<p>', 0, 403),
(408, '<h5>', 0, 403),
(409, '<thead>', 0, 403),
(410, '<h2>', 0, 403),
(413, '<p>', 0, 412),
(414, '<h1>', 0, 412),
(415, '<h2>', 0, 412),
(418, 'angular', 0, 417),
(419, 'react', 0, 417),
(420, 'vue', 0, 417),
(427, 'relationnel', 0, 426),
(428, 'no relationnel', 0, 426),
(429, 'fichier', 0, 426),
(432, '<footer>', 0, 431),
(433, '<title>', 0, 431),
(434, '<html>', 0, 431),
(435, '<p>', 0, 431),
(436, '<h5>', 0, 431),
(437, '<thead>', 0, 431),
(438, '<h2>', 0, 431),
(441, '<p>', 0, 440),
(442, '<h1>', 0, 440),
(443, '<h2>', 0, 440);

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(445),
(445),
(445),
(445),
(445),
(445),
(445),
(445),
(445),
(445),
(445),
(445);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL,
  `datet` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `media`
--

INSERT INTO `media` (`id`, `datet`) VALUES
(1, '2020-06-04 03:08:00');

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `desgn_question` text NOT NULL,
  `temps_question` int(11) NOT NULL,
  `unite` varchar(100) NOT NULL,
  `theme` varchar(100) NOT NULL,
  `etat_origin` int(11) NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=9419 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id_question`, `desgn_question`, `temps_question`, `unite`, `theme`, `etat_origin`) VALUES
(3, 'En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?', 1, 'minute', 'html5', 1),
(4, 'Lequel des élements suivants ne sont plus pris en charge dans HTML5', 2, 'minute', 'html5', 1),
(5, 'parmi les proposition suivantes lequels une Framework front end ?!', 2, 'minute', 'html5', 1),
(6, 'react est un ?', 1, 'minute', 'front', 1),
(7, 'l\'élement <canvas> en  HTML5 est utilisé pour: ?', 3, 'minute ', 'front', 1),
(144, 'MYSQL EST UN BASE DE DONNEES', 2, 'minute', 'BD', 1),
(350, 'Lequel des élements suivants ne sont plus pris en charge dans HTML5', 2, 'minute', '', 0),
(355, 'l\'élement <canvas> en  HTML5 est utilisé pour: ?', 3, 'minute ', 'systeme', 0),
(357, 'ES6', 2, 'minute', '', 0),
(403, 'En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?', 1, 'minute', 'html5', 0),
(412, 'Lequel des élements suivants ne sont plus pris en charge dans HTML5', 2, 'minute', 'html5', 0),
(417, 'parmi les proposition suivantes lequels une Framework front end ?!', 2, 'minute', 'html5', 0),
(422, 'react est un ?', 1, 'minute', 'front', 0),
(424, 'l\'élement <canvas> en  HTML5 est utilisé pour: ?', 3, 'minute ', 'front', 0),
(426, 'MYSQL EST UN BASE DE DONNEES', 2, 'minute', 'BD', 0),
(431, 'En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?', 1, 'minute', 'html5', 0),
(440, 'Lequel des élements suivants ne sont plus pris en charge dans HTML5', 2, 'minute', 'html5', 0),
(511, 'quelle est le type de jointure', 2, 'minute', 'BD', 1),
(4281, 'que signifie l\'erreur segmentation fault', 2, 'minute', 'systeme', 1),
(7021, 'ES6', 2, 'minute', 'javascript', 1),
(8157, 'c\'est quoi LDD', 4, 'minute', 'systeme', 1),
(8908, 'que fait select', 2, 'minute', 'BD', 1),
(9415, 'que fait le focntion mallaoc', 1, 'minute', 'C', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reponse_candidat`
--

DROP TABLE IF EXISTS `reponse_candidat`;
CREATE TABLE IF NOT EXISTS `reponse_candidat` (
  `id_reponse` int(11) NOT NULL AUTO_INCREMENT,
  `reponse` int(1) NOT NULL,
  PRIMARY KEY (`id_reponse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `reunion`
--

DROP TABLE IF EXISTS `reunion`;
CREATE TABLE IF NOT EXISTS `reunion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(200) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `end_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_all_day` tinyint(4) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `id_candidat` (`id_candidat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=1475 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reunion`
--

INSERT INTO `reunion` (`id`, `Subject`, `id_candidat`, `id_user`, `end_time`, `is_all_day`, `start_time`) VALUES
(1, 'mettig hamza ammar', 2, 5, '2020-06-02 02:07:14', 0, '2020-06-02 00:16:36'),
(2, 'mettig houssem ammar', 19, 5, '2020-06-03 02:16:36', 0, '2020-06-03 01:16:36'),
(3, 'metting sihem', 3, 5, '2020-06-04 14:48:02', 0, '2020-06-04 14:48:02'),
(24, 'test ', 18, 189, '2020-07-08 14:30:00', 0, '2020-07-08 14:13:00'),
(796, 'test js ', 2, 189, '2020-07-10 10:15:00', 0, '2020-07-10 10:03:00');

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id_test` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `logo` blob DEFAULT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `type_test` varchar(100) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  `score` float NOT NULL,
  `etat_test` int(11) NOT NULL,
  `date_evaluation` date DEFAULT NULL,
  `resultat` text NOT NULL,
  `etat_origin` int(11) NOT NULL,
  PRIMARY KEY (`id_test`),
  KEY `id_candidat` (`id_candidat`)
) ENGINE=InnoDB AUTO_INCREMENT=403 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `test`
--

INSERT INTO `test` (`id_test`, `theme`, `description`, `logo`, `date_debut`, `date_fin`, `type_test`, `id_candidat`, `score`, `etat_test`, `date_evaluation`, `resultat`, `etat_origin`) VALUES
(2, 'html5', 'HTML5', NULL, '2020-05-29', '2020-05-30', 'Technique', 19, 66.6667, 1, '2020-06-27', '[\n  {\n    \"id\": 3,\n    \"name\": \"En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 20,\n        \"questionId\": 3,\n        \"name\": \"\\u003cfooter\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 24,\n        \"questionId\": 3,\n        \"name\": \"\\u003ctitle\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 25,\n        \"questionId\": 3,\n        \"name\": \"\\u003chtml\\u003e\",\n        \"answer\": true,\n        \"selected\": true\n      },\n      {\n        \"id\": 26,\n        \"questionId\": 3,\n        \"name\": \"\\u003cp\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 33,\n        \"questionId\": 3,\n        \"name\": \"\\u003ch5\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 40,\n        \"questionId\": 3,\n        \"name\": \"\\u003cthead\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 45,\n        \"questionId\": 3,\n        \"name\": \"\\u003ch2\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      }\n    ]\n  },\n  {\n    \"id\": 4,\n    \"name\": \"Lequel des élements suivants ne sont plus pris en charge dans HTML5\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 6,\n        \"questionId\": 4,\n        \"name\": \"\\u003cp\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 7,\n        \"questionId\": 4,\n        \"name\": \"\\u003ch1\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 8,\n        \"questionId\": 4,\n        \"name\": \"\\u003ch2\\u003e\",\n        \"answer\": true,\n        \"selected\": true\n      }\n    ]\n  },\n  {\n    \"id\": 5,\n    \"name\": \"parmi les proposition suivantes lequels une Framework front end ?!\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 21,\n        \"questionId\": 5,\n        \"name\": \"angular\",\n        \"answer\": false,\n        \"selected\": true\n      },\n      {\n        \"id\": 22,\n        \"questionId\": 5,\n        \"name\": \"react\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 23,\n        \"questionId\": 5,\n        \"name\": \"vue\",\n        \"answer\": true,\n        \"selected\": false\n      }\n    ]\n  }\n]', 1),
(7, 'base de donness', 'relationnel', NULL, '2020-05-07', '2020-05-27', 'Technique', 2, 0, 0, NULL, 'no  result', 0),
(193, 'Test Mental', 'test lettres', NULL, '2020-06-10', '2020-06-24', 'Logique', 18, 0, 0, NULL, 'no  result', 1),
(194, 'react JS', 'les abc de react', NULL, '2020-06-09', '2020-06-17', 'Technique', 2, 0, 0, NULL, 'no  result', 1),
(195, 'SQl', 'algèbre Relationnel ', NULL, '2020-06-10', '2020-06-26', 'Logique', 2, 0, 0, NULL, 'no  result', 1),
(234, 'Test Hamza', 'hhh', NULL, '2020-06-15', '2020-06-15', 'Technique', 2, 0, 0, NULL, 'no  result', 1),
(312, 'PHP', 'description', NULL, '2020-06-15', '2020-06-16', 'Technique', 2, 0, 0, NULL, 'no  result', 1),
(387, 'html5', 'HTML5', NULL, '2020-06-16', '2020-06-24', 'Technique', 18, 0, 0, NULL, 'no resultat', 0),
(391, 'base de donness', 'relationnel', NULL, '2020-06-16', '2020-06-24', 'Logique', 18, 100, 1, '2020-06-16', '[\n  {\n    \"id\": 511,\n    \"name\": \"quelle est le type de jointure\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 27,\n        \"questionId\": 511,\n        \"name\": \"join\",\n        \"answer\": true,\n        \"selected\": true\n      },\n      {\n        \"id\": 28,\n        \"questionId\": 511,\n        \"name\": \"where\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 29,\n        \"questionId\": 511,\n        \"name\": \"having\",\n        \"answer\": false,\n        \"selected\": false\n      }\n    ]\n  },\n  {\n    \"id\": 144,\n    \"name\": \"MYSQL EST UN BASE DE DONNEES\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 30,\n        \"questionId\": 144,\n        \"name\": \"relationnel\",\n        \"answer\": true,\n        \"selected\": true\n      },\n      {\n        \"id\": 31,\n        \"questionId\": 144,\n        \"name\": \"no relationnel\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 32,\n        \"questionId\": 144,\n        \"name\": \"fichier\",\n        \"answer\": false,\n        \"selected\": false\n      }\n    ]\n  }\n]', 0),
(402, 'cobol', 'dede', NULL, '2020-07-07', '2020-07-21', 'Technique', 20, 0, 0, NULL, 'no  result', 1);

-- --------------------------------------------------------

--
-- Structure de la table `testquestion`
--

DROP TABLE IF EXISTS `testquestion`;
CREATE TABLE IF NOT EXISTS `testquestion` (
  `id_test_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  PRIMARY KEY (`id_test_question`),
  KEY `id_test` (`id_test`),
  KEY `id_question` (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=445 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `testquestion`
--

INSERT INTO `testquestion` (`id_test_question`, `id_test`, `id_question`) VALUES
(11, 2, 3),
(12, 2, 4),
(13, 2, 5),
(14, 7, 511),
(17, 7, 144),
(22, 195, 8908),
(23, 195, 8157),
(354, 194, 350),
(356, 194, 355),
(361, 194, 357),
(388, 387, 3),
(389, 387, 4),
(390, 387, 5),
(392, 391, 511),
(393, 391, 144),
(411, 402, 403),
(416, 402, 412),
(421, 402, 417),
(423, 402, 422),
(425, 402, 424),
(430, 402, 426),
(439, 402, 431),
(444, 402, 440);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastpasswordresetdate` datetime NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `useradresse` varchar(255) NOT NULL,
  `userdate` datetime DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `usertel` varchar(255) NOT NULL,
  `disponibilite` bit(1) DEFAULT NULL,
  `poste` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `email`, `enabled`, `firstname`, `lastpasswordresetdate`, `lastname`, `password`, `useradresse`, `userdate`, `username`, `usertel`, `disponibilite`, `poste`, `id`, `pwd`) VALUES
(3, 'tes@gmail.com', b'1', 'zohra', '2020-07-13 23:36:46', 'rebei', '$2a$10$v2oBeABa6Lsrr8yFor0fzONOvuRekzPgzzBZ62De9eqjY3fD9DcRe', 'tunis', '2020-05-23 18:00:53', 'zohra12345', '12345687', b'1', 'Admin', 187, 'zohra12345'),
(4, 'khaled@yahoo.com', b'1', 'khaled', '2020-07-14 14:27:04', 'rejeb', '$2a$10$ydPHbg5wDB.0jHaDsPA62eIBy.NJvIOnPAko5wZQkhkLju9HRD38a', 'tunis omran', '2020-05-23 18:08:11', 'admin', '25987419', b'1', NULL, 188, 'kaled12345'),
(5, 'mohamed@medianet.com', b'1', 'mohamed', '2020-07-13 23:34:57', 'hossinn', '$2a$10$2gquRJibBLIu7uD8i/gxROP1cZ4tMV0VS/Tp.lF.75agmg3.G9BUa', 'sfax', '2020-05-25 13:41:18', 'mohamed', '9287412', b'1', 'Admin', 189, 'med');

-- --------------------------------------------------------

--
-- Structure de la table `user_authority`
--

DROP TABLE IF EXISTS `user_authority`;
CREATE TABLE IF NOT EXISTS `user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  KEY `FKgvxjs381k6f48d5d2yi11uh89` (`authority_id`),
  KEY `FKpqlsjpkybgos9w2svcri7j8xy` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_seq`
--

DROP TABLE IF EXISTS `user_seq`;
CREATE TABLE IF NOT EXISTS `user_seq` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_seq`
--

INSERT INTO `user_seq` (`next_val`) VALUES
(5),
(5);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `candidat_reponse_choix`
--
ALTER TABLE `candidat_reponse_choix`
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_1` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id_candidat`),
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_2` FOREIGN KEY (`id_choix`) REFERENCES `choix` (`id_choix`),
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_3` FOREIGN KEY (`id_reponse`) REFERENCES `reponse_candidat` (`id_reponse`);

--
-- Contraintes pour la table `choix`
--
ALTER TABLE `choix`
  ADD CONSTRAINT `choix_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reunion`
--
ALTER TABLE `reunion`
  ADD CONSTRAINT `FK8hbq69tj2e2cc67q6ijayik7j` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id_candidat`);

--
-- Contraintes pour la table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id_candidat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `testquestion`
--
ALTER TABLE `testquestion`
  ADD CONSTRAINT `testquestion_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE,
  ADD CONSTRAINT `testquestion_ibfk_2` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user_authority`
--
ALTER TABLE `user_authority`
  ADD CONSTRAINT `FKgvxjs381k6f48d5d2yi11uh89` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`),
  ADD CONSTRAINT `FKpqlsjpkybgos9w2svcri7j8xy` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
